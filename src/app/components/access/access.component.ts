import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { Profile } from '../profile/profile.model';
import { ProfileService } from '../profile/profile.service';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.css']
})
export class AccessComponent implements OnInit {

  profiles: Profile[] = [];
  profile: Profile;
  id: string;
  user: Profile = {
    id: undefined,
    cpf: '00000000000',
    name: undefined,
    email: undefined,
    password: '00000000000',
    profile: 'admin'
  }

  constructor(
    private appService: AppService,
    private profileService: ProfileService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {
    this.activateRoute.params.subscribe(parametros => {
      this.id = parametros.id;
    })
  }

  ngOnInit(): void {
    this.profile = new Profile();
  }

  accessRegister(form: NgForm) {
    if (form.valid) {
      this.profile.id = Guid.create().toString();
      this.profileService.saveProfile(this.profile);
      this.appService.showMessage('O CPF ' + this.profile.cpf + ' foi registrado no sistema!');
      window.location.reload()
      this.router.navigate(['/']);
    } else {
      this.appService.showMessage('Todos os campos e valores precisam ser preenchidos!');
    }
  }

  accessLogin(form: NgForm) {
    if (form.valid) {
      if (form.value.cpf === this.user.cpf && form.value.password === this.user.password) {
        this.profiles = this.profileService.listProfile()
        sessionStorage.setItem('isLoggedIn', 'true');
        sessionStorage.setItem('profile', JSON.stringify(this.profile));
        this.appService.showMessage('Bem vindo!');
        this.router.navigate(['/']);
      } else {
        this.appService.showMessage('Ops! Algo de errado não está certo. Suas informações de acesso estão incorretas!');
        console.log();
      }
    } else {
      this.appService.showMessage('Todos os campos e valores precisam ser preenchidos!');
    }
  }
}
