import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Profile } from '../profile.model';
import { ProfileService } from '../profile.service';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-profile',
  templateUrl: './list-profile.component.html',
  styleUrls: ['./list-profile.component.css']
})
export class ListProfileComponent implements OnInit {

  profiles: Profile[] = [];
  profile: Profile;
  id: string;
  

  constructor(
    private appService: AppService,
    private profileService: ProfileService,
    private activateRoute: ActivatedRoute,
    private router: Router
  ) {
    this.activateRoute.params.subscribe(parametros => {
      this.id = parametros.id;
    })
  }

  ngOnInit(): void {
    this.profiles = this.profileService.listProfile();
  }

  displayedColumns: string[] = ['cpf', 'name', 'email', 'profile', 'edit', 'delete'];
  dataSource = new MatTableDataSource(this.profiles);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
