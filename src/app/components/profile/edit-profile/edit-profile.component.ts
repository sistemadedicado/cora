import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { Profile } from '../profile.model';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  profile: Profile;
  id: string;

  constructor(
    private appService: AppService,
    private profileService: ProfileService,
    private activateRoute: ActivatedRoute,
    private router: Router
  ) {
    this.activateRoute.params.subscribe(parametros => {
      this.id = parametros.id;
    })
  }

  ngOnInit(): void {
    this.profile = this.profileService.getProfile(this.id);
  }

  editProfile(form: NgForm) {
    if (form.valid) {
      this.profileService.saveProfile(this.profile);
      this.appService.showMessage('O perfil de '+ this.profile.name + ' foi atualizado com sucesso!');
      this.router.navigate(['/']);
    }
  }

  comeBack(): void {
    this.appService.showMessage('Mudou de ideia?');
    this.router.navigate(['/'])
  }

}
