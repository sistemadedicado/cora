import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { Profile } from '../profile.model';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-remove-profile',
  templateUrl: './remove-profile.component.html',
  styleUrls: ['./remove-profile.component.css']
})
export class RemoveProfileComponent implements OnInit {

  profile: Profile;
  id: string;

  constructor(
    private appService: AppService,
    private profileService: ProfileService,
    private activateRoute: ActivatedRoute,
    private router: Router
  ) {
    this.activateRoute.params.subscribe(parametros => {
      console.log(parametros);
      this.id = parametros.id;
    })
  }

  ngOnInit() {
    this.profile = this.profileService.getProfile(this.id);
  }

  removeProfile(form: NgForm) {
    this.profileService.removeProfile(this.profile);
    this.appService.showMessage('O perfil foi removido do sistema!');
    this.router.navigate(['/']);
  }

  comeBack(): void {
    this.appService.showMessage('Mudou de ideia?');
    this.router.navigate(['/'])
  }

}
