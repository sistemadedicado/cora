export class Profile {
    id: String;
    cpf: String;
    name: String;
    email: String;
    password: String;
    profile: String;
}
