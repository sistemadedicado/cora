import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import { AppService } from 'src/app/app.service';
import { Profile } from '../profile.model';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-add-profile',
  templateUrl: './add-profile.component.html',
  styleUrls: ['./add-profile.component.css']
})
export class AddProfileComponent implements OnInit {

  profile: Profile;

  constructor(
    private appService: AppService,
    private profileService: ProfileService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.profile = new Profile();
  }

  addProfile(form: NgForm) {
    if (form.valid) {
      this.profile.id = Guid.create().toString();
      this.profileService.saveProfile(this.profile);
      this.appService.showMessage('O CPF ' + this.profile.cpf + ' foi registrado no sistema!');
      this.router.navigate(['/']);
    } else {
      this.appService.showMessage('Todos os campos e valores precisam ser preenchidos!');
    }
  }

  comeBack(): void {
    this.appService.showMessage('Mudou de ideia?');
    this.router.navigate(['/'])
  }

}
