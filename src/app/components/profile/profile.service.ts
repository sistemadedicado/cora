import { Injectable } from '@angular/core';
import { Profile } from './profile.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor() { }

  saveProfile(profile: Profile) {
    localStorage.setItem(profile.id.toString(), JSON.stringify(profile));
  }

  getProfile(id: string): Profile {
    return JSON.parse(localStorage.getItem(id));
  }

  listProfile(): Array<Profile> {
    let profiles: Profile[] = [];
    for (let i = 0; i < localStorage.length; i++) {
      profiles.push(JSON.parse(localStorage.getItem(localStorage.key(i))))
    }
    return profiles;
  }

  removeProfile(profile: Profile) {
    localStorage.removeItem(profile.id.toString());
  }

}
