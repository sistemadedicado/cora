import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Profile } from './profile.model';
import { ProfileService } from './profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profile: Profile;
  id: string;

  constructor(
    private profileService: ProfileService,
    private appService: AppService
  ) { }

  ngOnInit() {
    this.profile = new Profile();
  }

}
