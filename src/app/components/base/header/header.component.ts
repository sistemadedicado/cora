import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  title = 'CORA'

  constructor(
    private router: Router,
    private appService: AppService
  ) { }

  status: boolean;

  ngOnInit() {
    this.status = false;      
    if (sessionStorage.getItem('isLoggedIn') == "true") {      
      this.status = true;
      console.log(this.status)
    } 
    else {      
      this.status = false;
      console.log(this.status)
    }
  }

  accessLogout() {
    this.router.navigate(['access']);
    sessionStorage.clear();
    this.appService.showMessage('Você saiu do sistema');
    window.location.reload()
  }

}
