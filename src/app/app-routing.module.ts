import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessComponent } from './components/access/access.component';
import { AuthGuard } from './components/access/guards/auth.guards';
import { AddProfileComponent } from './components/profile/add-profile/add-profile.component';
import { EditProfileComponent } from './components/profile/edit-profile/edit-profile.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RemoveProfileComponent } from './components/profile/remove-profile/remove-profile.component';

const routes: Routes = [
  { path: 'access', component: AccessComponent },
  { path: 'remove-profile/:id', component: RemoveProfileComponent, canActivate : [AuthGuard] },
  { path: 'edit-profile/:id', component: EditProfileComponent, canActivate : [AuthGuard] },
  { path: 'add-profile', component: AddProfileComponent, canActivate : [AuthGuard] },
  { path: '', component: ProfileComponent, canActivate : [AuthGuard] },
  { path: '**', redirectTo: '', pathMatch: 'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
