# Projeto CORA



<img src="https://gitlab.com/sistemadedicado/cora/-/blob/main/src/assets/img/cora_login.png" alt="CORA">

> Este projeto foi desenvolvido em Angular 7. O objetivo é que seja possível adicionar, listar, editar e remover perfis de usuários.

### Ajustes e melhorias

O projeto ainda não teve suas funcionalidades desenvolvidas completamente:

- [x] Páginas com proteção de Login
- [x] Sessão de Login
- [x] Dados salvos em localStorage
- [ ] Autenticação de Login com base em localSorage
- [ ] Filtro de pesquisa de Perfis
- [ ] Visualização diferenciada de Perfis

## 💻 Pré-requisitos para rodar aplicação em localhost

Antes de começar, verifique se você atendeu aos seguintes requisitos:
<!---Estes são apenas requisitos de exemplo. Adicionar, duplicar ou remover conforme necessário--->
* NodeJS 16.x `node --version && npm --version`

## 🚀 Instalando CORA em localhost

Para instalar o CORA, após fazer um clone do repositório, siga estas etapas:

Terminal Windows, Linux e macOS:
acesse a pasta principal do projeto e execute
```
npm i
```
Depois de ter completado a instalação dos ítens de repositório do angular, execute:
```
ng serve --open
```
Será aberto a tela inical da aplicação no navegador padrão do usuário local.

## ☕ Usando o CORA

Navegando no CORA será solicitado um CPF e Senha para acesso. O padrão da aplicação são 11 (onze) zeros para o campo CPF e Senha:

```
CPF: 00000000000
Senha: 00000000000
```

Será possível adicionar (cadastrar) novos perfis, editar e excluir.
